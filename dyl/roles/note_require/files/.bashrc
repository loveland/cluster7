# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

export EDITOR=vim
alias vi="vim"

HISTTIMEFORMAT="%d/%m/%y %T "