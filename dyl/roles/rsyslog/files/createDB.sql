CREATE TABLE SystemEvents
(
        ID bigint unsigned not null auto_increment primary key,
        CustomerID bigint,
        ReceivedAt datetime NULL,
        DeviceReportedTime datetime NULL,
        Facility smallint NULL,
        Priority smallint NULL,
        FromHost varchar(60) NULL,
        Message text,
        NTSeverity int NULL,
        Importance int NULL,
        EventSource varchar(60),
        EventUser varchar(60) NULL,
        EventCategory int NULL,
        EventID int NULL,
        EventBinaryData text NULL,
        MaxAvailable int NULL,
        CurrUsage int NULL,
        MinUsage int NULL,
        MaxUsage int NULL,
        InfoUnitID int NULL ,
        SysLogTag varchar(60),
        EventLogType varchar(60),
        GenericFileName VarChar(60),
        SystemID int NULL
) ENGINE=InnoDB;

CREATE TABLE SystemEventsProperties
(
        ID bigint unsigned not null auto_increment primary key,
        SystemEventID bigint NULL ,
        ParamName varchar(255) NULL ,
        ParamValue text NULL
) ENGINE=InnoDB;

CREATE TABLE `email_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `postfix_id` varchar(11) NOT NULL,
  `email_to` varchar(80) DEFAULT NULL,
  `email_from` varchar(80) DEFAULT NULL,
  `email_size` int(11) DEFAULT NULL,
  `relay` varchar(255) DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `last_status` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `postfix_id_1` (`postfix_id`),
  KEY `message_id_1` (`message_id`(32)),
  KEY `ts_1` (`ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `freeswitch_log` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(64) NOT NULL,
  `message` TEXT NOT NULL,
  `ts_gmt` datetime NOT NULL,
  `ts_millis` bigint NOT NULL,
  `uuid` varchar(36) NOT NULL,
  INDEX ts_gmt_1 (`ts_gmt`),
  INDEX uuid_1 (`uuid`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `freeswitch_cdr` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `caller_id_name` varchar(255) DEFAULT NULL,
  `caller_id_number` varchar(255) DEFAULT NULL,
  `destination_number` varchar(255) DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL,
  `ts_gmt` datetime DEFAULT NULL,
  `ts_answer` datetime DEFAULT NULL,
  `ts_end` datetime DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `billsec` int(11) DEFAULT NULL,
  `hangup_cause` varchar(50) DEFAULT NULL,
  `uuid` varchar(100) DEFAULT NULL,
  `bleg_uuid` varchar(100) DEFAULT NULL,
  `accountcode` varchar(10) DEFAULT NULL,
  `domain_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `bleg_uuid` (`bleg_uuid`),
  KEY `ts_gmt` (`ts_gmt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

