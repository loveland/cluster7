#!/bin/bash
# loveland

adminemail="nloveland@dyl.com"
sqlfile="/tmp/server_inventory.sql"
sqldb="server_inventory"

#mysqlhost="165.22.134.9"
#mysqluser="root"
#mysqlpw="mqRfBU_3Xk>r"

mysqlhost="localhost"
mysqluser="root"
mysqlpw="HZqPO5BW1&"


function isinstalled {
  if yum list installed "$@" >/dev/null 2>&1; then
    true
  else
    false
  fi
}

if isinstalled jq; then echo "jq ok"; else 'yum install jq -y'; fi

# Grab this server's public IP address
ip=$(curl https://ipinfo.io/ip)

# Call the geolocation API and capture the output
curl -s https://ipvigilante.com/$ip | \
        jq '.data.latitude, .data.longitude, .data.city_name, .data.subdivision_1_name, .data.country_name' | \
        while read -r LATITUDE
        do
                read -r LONGITUDE
                read -r CITY
                read -r COUNTRY
                read -r REGION
                whereami="($LATITUDE $LONGITUDE) $CITY, $COUNTRY $REGION" 
                echo $whereami > /tmp/geoloc.log
        done

whereami=$(cat /tmp/geoloc.log)
rm /tmp/geoloc.log
hostname=$(hostname)
pubip=$(curl ipecho.net/plain)
kernelver=$(uname -r)
os=$(hostnamectl | grep 'Operating System' | cut -c21-)
osver=$(rpm --query centos-release)
machinetype=$(hostnamectl | grep 'Chassis' | cut -c21-)
cpuspeed=$(lscpu | grep MHz | cut -c24-)
cpucount=$(nproc)
cputype=$(cat /proc/cpuinfo | grep 'model name' | uniq | cut -c14-)
serialnum=$(dmidecode -s system-serial-number)
disks=$(blkid -o device)
totaldiskspace=$(df -h --total | grep total | cut -c18- | cut -c -3)
#useddiskspace=$(df -h --total | grep total | cut -c23- | cut -c -4)
#remainingdiskspace=$(df -h --total | grep total | cut -c30- | cut -c -3)
cdate=$(ls -l /tmp/server_inventory.sql | cut -c29- | cut -c -12) 
creationdate=$(date -d "$cdate" +'%m-%d-%y %r')
modifieddate=$(date +"%m-%d-%y %r")

mysql -h$mysqlhost -u$mysqluser -p$mysqlpw server_inventory < $sqlfile 
id=$(mysql -s -N -h$mysqlhost -u$mysqluser -p$mysqlpw server_inventory --raw --batch -e 'select count(SerialNumber) from server_inventory' -s)

rm server_inventory_$hostname.sql

cat > "server_inventory_$hostname.sql" <<'endmsg'
INSERT INTO `server_inventory` (`id`, `Hostname`, `PublicIP`, `KernelVersion`, `OS`, `OSVer`, `MachineType`, `CPUCount`, `CPUSpeed`, `CPUType`, `Disks`, `TotalDiskSpace`, `SerialNumber`, `GeoLocation`, `CreationDate`, `ModifiedDate`)
endmsg

echo VALUES '('$id, "'"$hostname"'", "'"$pubip"'", "'"$kernelver"'", "'"$os"'", "'"$osver"'", "'"$machinetype"'", $cpucount, $cpuspeed, "'"$cputype"'", "'"$disks"'", "'"$totaldiskspace"'", $serialnum, "'"$whereami"'", "'"$creationdate"'", "'"$modifieddate"'"')'';' >> "server_inventory_$hostname.sql"

mysql -h$mysqlhost -u$mysqluser -p$mysqlpw $sqldb < "server_inventory_$hostname.sql" 

exit 0;
