##-- loveland sysinfo CREATE TABLE & INSERT VALS

CREATE DATABASE IF NOT EXISTS server_inventory;

CREATE TABLE IF NOT EXISTS `server_inventory` (
  `id` int(3),
  `Hostname` varchar(20),
  `PublicIP` varchar(16),
  `KernelVersion` varchar(30),
  `OS` varchar(25),
  `OSVer` varchar(50),
  `MachineType` varchar(6),
  `CPUCount` varchar(1),
  `CPUSpeed` varchar(8),
  `CPUType` varchar(50),
  `Disks` varchar(30),
  `TotalDiskSpace` varchar(5),
  `GeoLocation` varchar(70),
  `SerialNumber` varchar(12),
  `CreationDate` varchar(20),
  `ModifiedDate` varchar(20)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
