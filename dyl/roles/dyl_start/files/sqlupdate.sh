#!/bin/bash
#loveland
lasttbl=$(mysql --batch -u root --disable-column-names --execute "select id from dyl1.sys_version order by id desc limit 1")

cd /usr/local/cyber/data/sql/
lastfile=`exec ls $MY_DIR | sed 's/\([0-9]\+\).*/\1/g' | sort -n | tail -1`

if [ "$lasttbl" == "$lastfile" ]
then
    echo "The last table in the db references $lasttbl.sql which matches the last sql file in directory $lastfile.sql. You're all caught up!"
    exit 0
else
    while [[ "$lasttbl" != "$lastfile" ]]
    do
        lasttbl=$((lasttbl +1))
        echo "db now loading $lasttbl.sql"
        mysql dyl1 < $lasttbl.sql
        sleep 1
    done
fi

cd /usr/local/cyber/scripts
./redis_start.pl