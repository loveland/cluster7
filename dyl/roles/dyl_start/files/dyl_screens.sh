#!/bin/bash
echo
echo "DYL1 as note"
echo "Starting fs_pbx_worker, fs_pbx, fs_dyl_worker, fs_dyl, apns_queue_consume, and daemon..."
screen -ls | grep Detached | cut -d. -f1 | awk '{print $1}' | xargs kill #kill all detached screen sessions
perl /home/note/cyber/scripts/pbx_reload_all.pl
screen -S fs_pbx_worker -d -m /home/note/cyber/scripts/server/fs_pbx_worker.pl
sleep 8
screen -S fs_pbx -d -m /home/note/cyber/scripts/server/fs_pbx.pl
sleep 8
screen -S fs_dyl_worker -d -m /home/note/cyber/scripts/server/fs_dyl_worker.pl
sleep 8
screen -S fs_dyl -d -m /home/note/cyber/scripts/server/fs_dyl.pl
sleep 8
screen -S apns_queue_consume -d -m /home/note/cyber/scripts/server/apns_queue_consume.pl
sleep 8
screen -S daemon -d -m /home/note/cyber/scripts/daemon.pl
screen -list
echo 
echo "Successfully started dyl startup scripts..."
echo 
echo "done."
echo
