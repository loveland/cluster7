#!/bin/bash
cd /usr/local/src/freeswitch
./bootstrap.sh -j
./configure -C --enable-portable-binary --enable-sctp\
            --prefix=/usr --localstatedir=/var --sysconfdir=/etc \
            --with-gnu-ld --with-python --with-erlang --with-openssl \
            --enable-core-odbc-support --enable-zrtp \
            --enable-core-pgsql-support \
            --enable-static-v8 --disable-parallel-build-v8
make
make -j install
make -j cd-sounds-install
make -j cd-moh-install
cd /usr/local/src/freeswitch/libs/esl
make
make perlmod-install