#!/bin/bash
rsync -arv /usr/pbx_files/dyl_sounds/dyl_user_moh /usr/share/freeswitch/sounds/
chown -R freeswitch:daemon /usr/share/freeswitch/sounds/dyl_user_moh

rsync -arv /usr/pbx_files/dyl_sounds/en/us/callie/dyl /usr/share/freeswitch/sounds/en/us/callie
chown -R freeswitch:daemon /usr/share/freeswitch/sounds/en/us/callie/dyl

rsync -arv /usr/pbx_files/dyl_sounds/en/us/callie/dyl_vm /usr/share/freeswitch/sounds/en/us/callie
chown -R freeswitch:daemon /usr/share/freeswitch/sounds/en/us/callie/dyl_vm

