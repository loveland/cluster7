From: "${voicemail_account}" <voicemail@${voicemail_domain}>
Date: ${RFC2822_DATE}
To: <${voicemail_email}>
Subject: Voicemail
X-Priority: ${voicemail_priority}
X-Mailer: DYL

Content-Type: text/plain; charset=ISO-8859-1; Format=Flowed
Content-Disposition: inline
Content-Transfer-Encoding: 7bit

Mailbox: ${voicemail_account}
Domain: ${voicemail_domain}
Length: ${voicemail_message_len}
Name: ${voicemail_caller_id_name}
Number: ${voicemail_caller_id_number}
File: ${voicemail_file_path}
Priority: ${voicemail_priority}
Transfer: ${dyl_transfer_uuid}
UUID: ${uuid}

