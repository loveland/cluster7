From: "Mailbox ${voicemail_account}" <voicemail@${voicemail_domain}>
Date: ${RFC2822_DATE}
To: <${voicemail_notify_email}>
Subject: Voicemail | ${voicemail_caller_id_name} | ${voicemail_caller_id_number} | ${voicemail_message_len}
X-Priority: ${voicemail_priority}
X-Mailer: DYL

Content-Type: multipart/alternative; 
	boundary="000XXX000"

--000XXX000
Content-Type: text/plain; charset=ISO-8859-1; Format=Flowed
Content-Disposition: inline
Content-Transfer-Encoding: 7bit

Received: ${voicemail_time}
From: "Mailbox ${voicemail_account}" <voicemail@${voicemail_domain}>
Length: ${voicemail_message_len}
Mailbox: ${voicemail_account}

--000XXX000
Content-Type: text/html; charset=ISO-8859-1
Content-Disposition: inline
Content-Transfer-Encoding: 7bit

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Voicemail from "${voicemail_caller_id_name}" <${voicemail_caller_id_number}> ${voicemail_message_len}</title>
<meta content="text/html; charset=iso-8859-1" http-equiv="content-type"/>
</head>
<body>

<font face=arial>
<b>Voicemail From:</b> ${voicemail_caller_id_name}<br/>
<br/>
<b>Phone Number:</b> <A HREF="tel:${voicemail_caller_id_number}">${voicemail_caller_id_number}</A><br/>
<br/>
<b>Received:</b> ${voicemail_time}<br/>
<br/>
<b>Length:</b> ${voicemail_message_len}<br/>
<br/>
<b>Mailbox:</b> ${voicemail_account}<br/>
</font>

</body>
</html>
--000XXX000--
