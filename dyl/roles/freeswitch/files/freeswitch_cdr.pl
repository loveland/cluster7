#!/usr/bin/perl
use strict;
our $session;

use DBI;
use POSIX 'strftime';
use IO::All;
use JSON::XS;
use Data::Dumper;
use Text::CSV_XS;

my @cc  = ("killall", "-HUP", "freeswitch");
system(@cc) == 0 or die "$0: system @cc failed: $?";

open(CFG, '/root/freeswitch_cdr.json') or die("open: $!");
$/ = undef;
my $cfg = <CFG>;
my $dbcfg = decode_json($cfg);

#{
#	'host' => 'localhost',
#	'database' => 'syslog',
#	'user' => 'syslog',
#	'password' => '...',
#};

my $sqlcfg = $dbcfg->{'mysql'};
my $dsn = "DBI:mysql:database=$sqlcfg->{'database'};host=$sqlcfg->{'host'}";
my $dbh = DBI->connect($dsn, $sqlcfg->{'user'}, $sqlcfg->{'password'}) or die "$0: Couldn't connect to database: " . DBI->errstr();

my $csv = Text::CSV_XS->new ({ binary => 1, auto_diag => 1 });
my @files = glob("/var/log/freeswitch/cdr-csv/Master.csv.*");
$/ = "\n";
my $insert = q|INSERT INTO freeswitch_cdr (caller_id_name, caller_id_number, destination_number, context, ts_gmt, ts_answer, ts_end, duration, billsec, hangup_cause, uuid, bleg_uuid, accountcode, domain_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)|;
my $sth = $dbh->prepare($insert);
foreach my $file (@files)
{
	open my $fh, "<:encoding(utf8)", $file or die "Open failed($file): $!";
	my $x = 0;
	while (my $row = $csv->getline($fh))
	{
		foreach my $i (4..6)
		{
			unless (length($row->[$i]))
			{
				$row->[$i] = undef;
			}
		}
		$sth->execute(@{$row}[0..12], $dbcfg->{'hostname'}); # will warn of errors
	}
	close($fh);
	unlink $file;
}

