## DEFAULTS ##

ls='ls --color=tty'
grep='grep  --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
ZSH_THEME="dpoggi"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="mm/dd/yyyy"
HISTFILESIZE=1000000000 HISTSIZE=1000000
ZSH="$HOME/.oh-my-zsh"
source $ZSH/oh-my-zsh.sh

## PLUGIN ##

plugins=(
colorize 
brew
cpanm
gem
pip
python
yum
)

## EXPORT ##

export ANSIBLE_NOCOWS=1
export CHROME_BOOKMARK="$HOME/AppData/Local/Google/Chrome/User Data/Default/Bookmarks"
export EDITOR="vim"
export LANG=en_US.UTF-8
export MANPATH="/usr/local/man:$MANPATH"
export PATH="$PATH:$HOME/.rvm/bin"
export PATH="$PATH:/usr/local/bin/gcc"
export PATH="${PATH:+${PATH}:}$HOME/bin"
export PATH="/usr/local/opt/python/libexec/bin:$PATH"
export PATH="/usr/local/opt/ruby/bin:$PATH"
export PATH="/usr/local/opt/ruby/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH=$PATH:~/bin
export PATH=/usr/local/opt/gnu-sed/libexec/gnubin:$PATH
export SSH_KEY_PATH="~/.ssh/id_rsa.pub"
export superexit=$(ps -ax | awk '! /awk/ && /Terminal/ { print $1}')
export UPDATE_ZSH_DAYS=7
export zsh="$HOME/.oh-my-zsh"
export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/usr/local/share/zsh-syntax-highlighting/highlighters

if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
fi

## ALIAS ##

alias ali='echo "line 1" >> greetings.txt'
alias ap='ansible-playbook -i ../hosts'
alias avd='ansible-vault decrypt '
alias ave='ansible-vault encrypt '
alias bmcsync='cp "$CHROME_BOOKMARK" "/d/someRepo/b-1 backup/5-1 bookmark/"'
alias cats='cat ~/.ssh/id_rsa.pub'
alias certexp='"openssl x509 -enddate -noout -in "/etc/letsencrypt/live/{{ansible_fqdn}}/fullchain.pem" | cut -c 10-"'
alias chownme='chown -R $whoami:$whoami'
alias cp='rsync -a --stats --progress'
alias cs='printf "Filename: "; read filename && ln -s $filename /Users/DYL/Desktop/$filename'
alias d='cd ~/Desktop'
alias ds="find . -name '.DS_Store' -type f -delete && curl -sm 30 k.wdt.io/nloveland@dyl.com/rm_ds_store_files?c=0_*/6_*_*_*"
alias dyl-sales='cd /Users/DYL/gitclones/loveland/scripts/ && ./dyl-sales.exp'
alias dyl.email='cd /Users/DYL/gitclones/loveland/scripts/ && ./dyl.email.exp'
alias dyl.services='cd /Users/DYL/gitclones/loveland/scripts/ && ./dyl.services.exp'
alias dylv='export ANSIBLE_VAULT_PASSWORD_FILE=/Users/DYL/tmp/vp/conf/dylv'
alias gam='~/bin/gam/gam'
alias hogs='du -ah / | sort -rn | head -n 10'
alias i2y='/Users/DYL/gitclones/ansible-ini2yaml/ini2yaml'
alias l='colorls --group-directories-first --almost-all'
alias ll='colorls --group-directories-first --almost-all --long' # detailed list view
alias mon1='ssh nloveland@mon1.revalead.com'
alias mpw="echo 'QcXEdylJokFjLzgdRckUPV8hh'"
alias my2='cd /Users/DYL/gitclones/loveland/cluster7/nl/dyl'
alias my3='cd /Users/DYL/gitclones/loveland/scripts'
alias my='cd /Users/DYL/gitclones/loveland/mail/install'
alias nods="sudo find / -name ".DS_Store" -delete"
alias pip='pip3'
alias pm='postfix flush && mailq | mailq >> /tmp/mailq.log && postsuper -d ALL deferred'
alias python='python3'
alias q='kill -9 $superexit'
alias rmsnaps='for d in $(tmutil listlocalsnapshotdates); do sudo tmutil deletelocalsnapshots $d; done'
alias rmtmp="find . -type f -name '._*' -delete"
alias s='clear && source ~/.zshrc'
alias sfv='export ANSIBLE_VAULT_PASSWORD_FILE=/Users/DYL/tmp/vp/conf/sfv'
alias sil='ssh nloveland@silver.revalead.com'
alias z="vi ~/.zshrc"

if [ -f '/Users/DYL/gitclones/mail/install/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/DYL/gitclones/mail/install/google-cloud-sdk/path.zsh.inc'; fi
if [ -f '/Users/DYL/gitclones/mail/install/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/DYL/gitclones/mail/install/google-cloud-sdk/completion.zsh.inc'; fi


##FUNCTION## 

pathadd() {
	newelement=${1%/}
	if [ -d "$1" ] && ! echo $PATH | grep -E -q "(^|:)$newelement($|:)" ; then
	  if [ "$2" = "after" ] ; then
			PATH="$PATH:$newelement"
	  else
			PATH="$newelement:$PATH"
	  fi
	fi
}

pathrm() {
	 PATH="$(echo $PATH | sed -e "s;\(^\|:\)${1%/}\(:\|\$\);\1\2;g" -e 's;^:\|:$;;g' -e 's;::;:;g')"
}

ssh-add()
{
		  command ssh-add $@ < /dev/null
}

load-nvmrc() {
    local node_version="$(nvm version)" # Current node version
    local nvmrc_path="$(nvm_find_nvmrc)" # Path to the .nvmrc file

    # Check if there exists a .nvmrc file
    if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    # Check if the node version in .nvmrc is installed on the computer
    if [ "$nvmrc_node_version" = "N/A" ]; then
        # Install the node version in .nvmrc on the computer and switch to that node version
        nvm install
    # Check if the current node version matches the version in .nvmrc
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
        # Switch node versions
        nvm use
    fi
    # If there isn't an .nvmrc make sure to set the current node version to the default node version
    elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
    fi
}

prev(){
    open -a /Applications/Preview.app/ "$1"
}


## PROFILE ## 

fortune | cowsay | lolcat && echo