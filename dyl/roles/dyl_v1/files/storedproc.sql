delimiter //

CREATE DEFINER=`root`@`localhost` FUNCTION `geodistance`(
 lat1  numeric (9,6),
 lon1  numeric (9,6),
 lat2  numeric (9,6),
 lon2  numeric (9,6)
) RETURNS decimal(10,5) DETERMINISTIC
BEGIN
  DECLARE  x  decimal (20,10);
  DECLARE  pi  decimal (21,20);
  SET  pi = 3.14159265358979323846;
  SET  x = sin( lat1 * pi/180 ) * sin( lat2 * pi/180  ) + cos(
 lat1 *pi/180 ) * cos( lat2 * pi/180 ) * cos(  abs ( (lon2 * pi/180) -
 (lon1 *pi/180) ) );
  SET  x = atan( ( sqrt( 1- power( x, 2 ) ) ) / x );
  RETURN  ( 1.852 * 60.0 * ((x/pi)*180) ) / 1.609344;
END
//

CREATE DEFINER=`root`@`localhost` FUNCTION `nextid`() RETURNS bigint(20) DETERMINISTIC
begin
 declare newid bigint;
 update sys_sequence set id = last_insert_id( id + 3 );
 select LAST_INSERT_ID() into newid;
 return newid;
end
//

CREATE DEFINER=`root`@`localhost` FUNCTION `zipdistance`(
    zipa  varchar(5),
    zipb  varchar(5)
) RETURNS decimal(10,5) DETERMINISTIC
BEGIN
 DECLARE  lat1  decimal (5,2);
 DECLARE  long1  decimal (5,2);
 DECLARE  lat2  decimal (5,2);
 DECLARE  long2  decimal (5,2);
 DECLARE  dist  decimal (5,2);
 SELECT  lat,lon  into  lat1,long1  FROM  rec_zip  WHERE  zip = zipa;
 SELECT  lat,lon  into  lat2,long2  FROM  rec_zip  WHERE  zip = zipb;
 set dist = geodistance(lat1,long1,lat2,long2);
 return dist;
END
//

CREATE DEFINER=`root`@`localhost` PROCEDURE `nearbyzip`(
    zipbase varchar(5),
    distance numeric(15)
)
BEGIN
DECLARE  lat1  decimal (5,2);
DECLARE  long1  decimal (5,2);
DECLARE  rangeFactor  decimal (7,6);
SET  rangeFactor = 0.014457;
SELECT  lat,lon into lat1,long1  FROM  rec_zip  WHERE  zip = zipbase;
SELECT  B.zip FROM rec_zip AS B WHERE
    B.lat  BETWEEN  lat1-(distance*rangeFactor)  AND  lat1+(distance*rangeFactor) AND
    B.lon  BETWEEN  long1-(distance*rangeFactor)  AND  long1+(distance*rangeFactor) AND
    geodistance(lat1,long1,B.lat,B.lon)  <= distance;
END
//

CREATE DEFINER=`root`@`localhost` PROCEDURE `nearbyzipstate`(
    zipbase varchar(5),
    distance numeric(15)
)
BEGIN
DECLARE  lat1  decimal (5,2);
DECLARE  long1  decimal (5,2);
DECLARE  state1  varchar(2);
DECLARE  rangeFactor  decimal (7,6);
SET  rangeFactor = 0.014457;
SELECT  lat,lon,state into lat1,long1,state1  FROM  rec_zip  WHERE  zip = zipbase;
SELECT  B.zip FROM rec_zip AS B WHERE
    B.lat  BETWEEN  lat1-(distance*rangeFactor)  AND  lat1+(distance*rangeFactor) AND
    B.lon  BETWEEN  long1-(distance*rangeFactor)  AND  long1+(distance*rangeFactor) AND
    geodistance(lat1,long1,B.lat,B.lon)  <= distance AND B.state = state1;
END
//

