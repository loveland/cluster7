package Cyber::Site;

use strict;
use warnings;

use Apache2::RequestRec ();
use Apache2::RequestIO ();
use Template;
use Data::Dumper;

use Apache2::Const -compile => 'OK';
our $tt_root = '/var/www/{{ hostname }}/public_html';
sub handler {
	my $r = shift;
	$r->content_type('text/html');
	my $real_url = lc($r->unparsed_uri());
	$real_url =~ s/\?.*//;
	my $path = $tt_root . $real_url;
	if (-d $path)
	{
		$path .= '/index.html';
		$real_url .= '/index.html';
	}
	unless (-f $path)
	{
		$real_url = '/404/index.html';
		$r->status(404);
	}
	my $vars = {
		'host' => 'tr.dyl.com',
		'url' => $real_url
	};
	my $tt = new Template({
		'INCLUDE_PATH' => $tt_root,
	});
	$real_url =~ s!//+!/!g;
	$real_url = substr($real_url, 1);
	$tt->process($real_url, $vars) || die $tt->error();
	return Apache2::Const::OK;
}

1;