#/bin/bash
cd /tmp
wget http://www.dcc-servers.net/dcc/source/dcc-dccproc.tar.Z
tar xzvf dcc-dccproc.tar.Z
cd dcc-dccproc-1*
./configure --with-uid=spamd
make
make install
chown -R spamd:spamd /var/dcc
ln -s /var/dcc/libexec/dccifd /usr/local/bin/dccifd