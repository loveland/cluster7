#/bin/bash
VBoxManage createvm --name "m1" --register
VBoxManage modifyvm "m1" --memory 1024 --acpi on --boot1 dvd --nic1 bridged --bridgeadapter1 bond0 --ostype Fedora_64
VBoxManage createvdi --filename ~/VirtualBox\ VMs/m1/m1-disk01.vdi --size 10000
VBoxManage storagectl "m1" --name "IDE Controller" --add ide
VBoxManage storageattach "m1" --storagectl "IDE Controller" --port 0 --device 0 --type hdd --medium ~/VirtualBox\ VMs/m1/m1-disk01.vdi
VBoxManage storageattach "m1" --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium ~/CentOS-7-x86_64-Minimal-1804.iso