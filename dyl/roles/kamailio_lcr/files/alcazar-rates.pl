#!/usr/bin/perl -w
use strict;
use DBI;
use JSON::XS;
use IO::All;

#------------------------------------------------------------------------
# open file $ARGV[0]
#------------------------------------------------------------------------

open(DATAFILE, "< $ARGV[0]") or die "\n", $!, "\ncould not open file.\n\n";

#------------------------------------------------------------------------
# check $ARGV[1] (carrierid)
#------------------------------------------------------------------------

unless ($ARGV[1] =~ /^\d+$/)
{
	die("Invalid carrier id");
}
my $carrierid = $ARGV[1];

#------------------------------------------------------------------------
# read first line and make it header array
#------------------------------------------------------------------------

my $header = <DATAFILE>;
chomp($header);
$header = lc($header);
$header =~ s/[\r]//g;
my @keys = split(",", $header);
close (DATAFILE);

#------------------------------------------------------------------------
# Connect to DB
#------------------------------------------------------------------------

my $cfgdata < io('lcr_database.json');
my $config = decode_json($cfgdata);
my $data_source = qq/DBI:mysql:database=$config->{'lcr_db_name'};host=$config->{'lcr_db_host'}/;
my $dbh = DBI->connect($data_source, $config->{'lcr_db_user'}, $config->{'lcr_db_password'}) or die qq(ERROR CONNECTING TO DATABASE\n);

#------------------------------------------------------------------------
# Parse each file line by line
#------------------------------------------------------------------------

my $counter = 0;
my $errorcount = 0;
my ($result, $sql, $sth);

open(DATAFILE, "< $ARGV[0]") or die "\n", $!, "\ncould not open file.\n\n";
while (<DATAFILE>) {
	unless ($counter == 0) {
		my ($line) = $_;
		chomp($line);
		$line =~ s/[\r]//g;
		$line =~ s/(".*),(.*")/$1 $2/g;
		$line =~ s/\'//g;
		$line =~ s/\"//g;
		my (@vals) = split(",",$line);
		my %hash;
		@hash{@keys} = @vals;
		#print "\n";
		my $error;

                unless ($hash{"interstate"}) { $error .= "No Interstate Rate. " }
                unless ($hash{"intrastate"}) { $error .= "No Intrastate Rate. " }
		unless ($hash{"destination"}) { $error .= "No Destination. " }
		if ($error) {
			$result .= "Error Line $counter: $error\n";
			++$errorcount;
		} else {

			$sql = qq^call update_rate($carrierid,"USA - $hash{'destination'}","$hash{'destination'}",$hash{'interstate'},$hash{'intrastate'})^;
			$sth = $dbh->prepare($sql) or die qq(ERROR PREPARING STATEMENT: $DBI::errstr\n);
			$sth->execute();			
			$sth->finish();
			print "$sql\n\n";
		}
	}
	++$counter;
}


#------------------------------------------------------------------------
# Close file / Disconnect DB
#------------------------------------------------------------------------

close (DATAFILE);

$dbh->disconnect;

print "Processed: $ARGV[0]\nLines: $counter\n";
print "Errors: $errorcount\n";
if ($result) {
		print "Results:\n$result";
	} else {
		print "No errors\n";
}
