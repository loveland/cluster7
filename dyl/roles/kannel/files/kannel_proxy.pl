#!/usr/bin/perl
use warnings;
use strict;

#use DBI;
use HTTP::Daemon;
use Data::Dumper;
use LWP::UserAgent;
use JSON::XS 'decode_json';
use DBIx::Connector;
use Socket 'inet_ntoa';

$SIG{'CHLD'} = 'IGNORE';

our $default_sms = 'http://dyl.com/post/receive_sms';
our $default_mms = 'http://dyl.com/system/mms';
our $ip;

my $d = new HTTP::Daemon(
	'LocalAddr' => '127.0.0.1', # localhost ONLY!
	'LocalPort' => '8081',
) or die "Unable To Bind";

open(CFG, 'proxy_cfg.json') or die("open: $!");
$/ = undef;
my $cfg = <CFG>;
my $dbcfg = decode_json($cfg);
my $dsn = "DBI:mysql:database=$dbcfg->{'database'};host=$dbcfg->{'host'}";
our $dbh = new DBIx::Connector($dsn, $dbcfg->{'user'}, $dbcfg->{'password'}, {'RaiseError' => 1, 'AutoCommit' => 1});

while (my $c = $d->accept())
{
	if (! fork())
	{
		$ip = $c->peerhost();
		if (my $r = $c->get_request())
		{
			#print STDERR "Path: ". $r->uri()->path(). "\n";
			if ($r->method() eq 'GET')
			{
				if ($r->uri()->path() eq '/receive_sms')
				{
					my %form = $r->uri()->query_form();
					#print STDERR Dumper(\%form);
					proxy_sms_request(\%form);
					my $rsp = new HTTP::Response(200);
					$rsp->content('OK');
					$c->send_response($rsp);
				}
				else
				{
					my $rsp = new HTTP::Response(500);
					$rsp->content('Error');
					$c->send_response($rsp);
				}
			}
			elsif ($r->method() eq 'POST')
			{
				if ($r->uri()->path() eq '/receive_mms')
				{
					proxy_mms_request($r);
					my $rsp = new HTTP::Response(200);
					$rsp->content('OK');
					$c->send_response($rsp);
				}
			}
			else
			{
				my $rsp = new HTTP::Response(500);
				$rsp->content('Error');
				$c->send_response($rsp);
			}
		}
		$c->close();
		undef($c);
		exit(0);
	}
}

sub proxy_sms_request
{
	my ($data) = @_;
	my $ua = new LWP::UserAgent();
	$ua->timeout(5);
	my $target = $default_sms;
	my $lk = '';
	eval {
		$lk = lookup_did($data->{'to'});
	};
	if ($@)
	{
		print STDERR $@;
	}
	#print STDERR "Lookup($data->{'to'}): $lk\n";
	if (length($lk))
	{
		$target = 'http://'. $lk. '/post/receive_sms';
	}
	my $r = $ua->post($target, $data);
	print "Client: $ip To: $data->{'to'} Target: $target Status: ". $r->status_line(). "\n";
	#print STDERR "Proxy To: $target\n";
	#print STDERR $r->status_line(). "\n";
	#print STDERR $r->content(). "\n";
}

sub proxy_mms_request
{
	my ($req) = @_;
	my $to = $req->header('x-mbuni-to');
	my $ua = new LWP::UserAgent();
	$ua->timeout(5);
	my $target = $default_mms;
	my $lk = '';
	eval {
		$lk = lookup_did($to);
	};
	if ($@)
	{
		print STDERR $@;
	}
	#print STDERR "Lookup($to): $lk\n";
	if (length($lk))
	{
		$target = 'http://'. $lk. '/system/mms';
	}
	my $nreq = HTTP::Request->new('POST' => $target);
	$nreq->content($req->content());
	foreach my $i (qw/
		Content-Type
		X-Mbuni-To
		X-Mbuni-From
		X-Mbuni-Received-Date
		X-Mbuni-TransactionID
		X-Mbuni-Message-Date
		X-Mbuni-Message-ID
		X-Mbuni-Subject
		X-Mbuni-MMSC-ID
		X-Mbuni-LinkedID
	/) {
		$nreq->header($i, $req->header(lc($i)));
	}
	#print STDERR "Request: ". Dumper($nreq);
	my $r = $ua->request($nreq);
	print "Client: $ip To: $to Target: $target Status: ". $r->status_line(). "\n";
	#print STDERR Dumper($r);
	#print STDERR "Reply:\n". $r->content(). "\n";
}

sub lookup_did
{
	my ($did) = @_;
	$did =~ s/\D//g;
	my $sth = $dbh->run('fixup' => sub {
		my $s = $_->prepare('select sms_host from did_sms where did=?');
		$s->execute($did);
		$s;
	});
	my $q = $sth->fetchall_arrayref();
	return '' unless (scalar @$q);
	$q->[0]->[0];
}

