##########################################################################
# Kamailio 3.x configuration basis - dialog tracking feature parameters. #
# 									 #
# For use by Evariste Systems LLC and authorised third parties only.     #
#									 #
# Boilerplate parameters, code and comment annotations by:               #
#									 #
# Alex Balashov <abalashov@evaristesys.com>                              #
##########################################################################

#!ifdef WITH_DIALOG_TRACKING

##
## Feature: WITH_DIALOG_TRACKING (concurrent dialog limits).
##

# Enable dialog tracking on inbound calls.  
# This really means what it says;  dialog _tracking_.  It has nothing to do
# with profile classification or concurrent channel limit checks or 
# enforcement.

dialog_tracking.enabled_on_outbound = 1 descr "Enable dialog tracking on outbound calls (boolean: 1 or 0)"

# Enable dialog tracking on inbound calls.  
# This really means what it says;  dialog _tracking_.  It has nothing to do
# with profile classification or concurrent channel limit checks or 
# enforcement.

dialog_tracking.enabled_on_inbound = 0 descr "Enable dialog limits on inbound calls (boolean: 1 or 0)"

# If enabled, this feature causes a bidirectional BYE to be spoofed upon 
# dialog timeout.  
#
# This requires special handling for accounting because the BYE is silently 
# generated without being exposed as a sequential request in script.

dialog_tracking.disconnect_on_timeout = 1 descr "Enable termination of call on dialog timeout (boolean: 1 or 0)"

# Enable per-vendor gateway dialog tracking for the purpose of enforcing
# the channel_limit field in the vendor_gw table.  This would be desirable
# in a situation where, for example, the vendor offers multiple POPs 
# with a concurrent call limit for each, requiring the proxy to spread calls
# across multiple POPs for load distribution or capacity management.

dialog_tracking.vendor_gw_chan_limit = 1 descr "Enable concurrent call limits on vendor gateways"

# Enable per-BG dialog tracking (customer_bg.channel_limit field) for the
# purpose of restricting concurrent calls on retail side.

dialog_tracking.bg_chan_limit = 1 descr "Enable concurrent call limits on billing groups"

#!endif
