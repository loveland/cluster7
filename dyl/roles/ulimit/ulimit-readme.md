Role for ulimit conf:


# Example 
- hosts: myhost
  vars:
    ulimit_config:
      - domain: '*'
        type: soft
        item: core
        value: 0
      - domain: '*'
        type: hard
        item: rss
        value: 10000
  roles:
    - ulimit
```

vars used by role:

```
# Default ulimit configuration
ulimit_config: []

# Default limits.conf location
ulimit_config_location: /etc/security/limits.conf

