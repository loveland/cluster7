#!/usr/bin/perl
use strict;
our $session;

use DBI;
use POSIX 'strftime';
use IO::All;
use JSON::XS;
use Data::Dumper;

open(CFG, 'freeswitch_summ_cfg.json') or die("open: $!");
$/ = undef;
my $cfg = <CFG>;
my $dbcfg = decode_json($cfg);

#{
#	'host' => 'localhost',
#	'database' => 'syslog',
#	'user' => 'syslog',
#	'password' => '...',
#};

my $dsn = "DBI:mysql:database=$dbcfg->{'database'};host=$dbcfg->{'host'}";
my $dbh = DBI->connect($dsn, $dbcfg->{'user'}, $dbcfg->{'password'}) or die "$0: Couldn't connect to database: " . DBI->errstr();

my $file = 'freeswitch_lastid.txt';
my $last_id = 0;
if (-e $file)
{
	io($file) > $last_id;
}

my $stm = q|SELECT ID, ReceivedAt, Message, FromHost FROM SystemEvents WHERE ID > ? AND SysLogTag LIKE 'freeswitch%' ORDER BY ID ASC|;
my $ul = $dbh->prepare($stm) or die "$0: Couldn't prepare statement $stm: " . $dbh->errstr();
$ul->execute($last_id) or die "$0: SQL Error: ". $dbh->errstr();
my $res = $dbh->selectall_arrayref($ul);
my $insert = q|INSERT INTO freeswitch_log (id, uuid, hostname, ts_gmt, ts_millis, message) VALUES (?, ?, ?, ?, ?, ?)|;
my $sth = $dbh->prepare($insert);

foreach my $r (@$res)
{
	if ($r->[2] =~ /^\s([\w\-]{36}) ([\d\-\:\s]{19})\.(\d+) (.*)$/)
	{
		unless ($sth->execute($r->[0], $1, $r->[3], $2, $3, $4))
		{
			print "$0: SQL Error: ". $dbh->errstr(). "\n";
		}
	}
	elsif ($r->[2] =~ /^\s+([\d\-\:\s]{19})\.(\d+).*?\[([\w\-]{36})\]\s*(.*)$/)
	{
		unless ($sth->execute($r->[0], $3, $r->[3], $1, $2, $4))
		{
			print "$0: SQL Error: ". $dbh->errstr(). "\n";
		}
	}
}

if (scalar(@$res))
{
	$last_id = $res->[-1]->[0];
	$last_id > io($file);
}

