#!/bin/bash

IP="54.153.44.185"
ENVIRON="staging"

# LCR Gateways ACL Entry (trusted)
ansible -u root -m shell -i $ENVIRON lcr_hosts -a "echo \"DELETE FROM trusted WHERE src_ip='$IP';\" | mysql openser; echo LCR ACL; kamctl trusted reload; kamctl trusted show"

# Inbound Gateway Route (dispatcher)
ansible -u root -m shell -i $ENVIRON dial_control_hosts -a "echo \"DELETE FROM dispatcher WHERE destination='sip:$IP:5080';\" | mysql inbound;"
ansible -u root -m shell -i $ENVIRON inbound_hosts -a "echo Inbound Dispatcher; kamctl dispatcher reload; kamctl dispatcher dump"

# Phone Gateway Routes (trusted, dispatcher)
ansible -u root -m shell -i $ENVIRON registrar_hosts -a "echo \"DELETE FROM dispatcher WHERE destination='sip:$IP:5090;transport=tcp';\" | mysql openser; echo Registrar Dispatcher; kamctl dispatcher reload; kamctl dispatcher show; echo \"DELETE FROM trusted WHERE src_ip='$IP';\" | mysql openser; echo Registrar ACL; kamctl trusted reload; kamctl trusted show" 
