#!/bin/bash

DNS="pbx3.dylphones.com"
IP="54.153.44.185"
PBX="2"
ENVIRON="staging"

# LCR Gateways ACL Entry (trusted)
ansible -u root -m shell -i $ENVIRON lcr_hosts -a "echo \"INSERT INTO trusted VALUES (NULL,'$IP','any',NULL,NULL);\" | mysql openser; echo LCR ACL; kamctl trusted reload; kamctl trusted show"

# Inbound Gateway Route (dispatcher)
ansible -u root -m shell -i $ENVIRON dial_control_hosts -a "echo \"INSERT INTO dispatcher VALUES (NULL,$PBX,'sip:$IP:5080',0,0,'','$DNS');\" | mysql inbound;"
ansible -u root -m shell -i $ENVIRON inbound_hosts -a "echo Inbound Dispatcher; kamctl dispatcher reload; kamctl dispatcher dump"

# Phone Gateway Routes (trusted, dispatcher)
ansible -u root -m shell -i $ENVIRON registrar_hosts -a "echo \"INSERT INTO dispatcher VALUES (NULL,$PBX,'sip:$IP:5090;transport=tcp',0,0,'','$DNS');\" | mysql openser; echo Registrar Dispatcher; kamctl dispatcher reload; kamctl dispatcher show; echo \"INSERT INTO trusted VALUES (NULL,'$IP','any',NULL,NULL);\" | mysql openser; echo Registrar ACL; kamctl trusted reload; kamctl trusted show" 
